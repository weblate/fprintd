# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Fran Diéguez <frandieguez@ubuntu.com>, 2012-2013
msgid ""
msgstr ""
"Project-Id-Version: fprintd\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-06-30 14:00+0200\n"
"PO-Revision-Date: 2017-09-23 18:02+0000\n"
"Last-Translator: Fran Diéguez <frandieguez@ubuntu.com>\n"
"Language-Team: Galician (http://www.transifex.com/freedesktop/fprintd/"
"language/gl/)\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: data/net.reactivated.fprint.device.policy.in:13
msgid "Verify a fingerprint"
msgstr "Verificar a pegada dixital"

#: data/net.reactivated.fprint.device.policy.in:14
msgid "Privileges are required to verify fingerprints."
msgstr "Requírense privilexios para verificar pegadas dixitais."

#: data/net.reactivated.fprint.device.policy.in:23
msgid "Enroll new fingerprints"
msgstr "Rexistrar novas pegadas dixitais"

#: data/net.reactivated.fprint.device.policy.in:24
msgid "Privileges are required to enroll new fingerprints."
msgstr "Requírense privilexios para rexistrar novas pegadas dixitais."

#: data/net.reactivated.fprint.device.policy.in:33
msgid "Select a user to enroll"
msgstr "Seleccione o usuario a rexistrar"

#: data/net.reactivated.fprint.device.policy.in:34
msgid "Privileges are required to enroll new fingerprints for other users."
msgstr ""
"Requírense privilexios para rexistrar novas pegadas dixitais para outros "
"usuarios."

#: src/device.c:690
#, c-format
msgid "Device was not claimed before use"
msgstr "O dispositivo non foi solicitado antes do seu uso"

#: src/device.c:699
#, c-format
msgid "Device already in use by another user"
msgstr "O dispositivo está sendo usado por outro usuario"

#: pam/fingerprint-strings.h:50
msgid "Place your finger on the fingerprint reader"
msgstr "Poña o seu dedo sobre o lector de pegadas dixitais"

#: pam/fingerprint-strings.h:51
#, c-format
msgid "Place your finger on %s"
msgstr "Poña o seu dedo en %s"

#: pam/fingerprint-strings.h:52
msgid "Swipe your finger across the fingerprint reader"
msgstr "Pase o seu dedo sobre o lector de pegadas dixitais"

#: pam/fingerprint-strings.h:53
#, c-format
msgid "Swipe your finger across %s"
msgstr "Pase o seu dedo sobre %s"

#: pam/fingerprint-strings.h:55
msgid "Place your left thumb on the fingerprint reader"
msgstr "Poña o seu dedo gordo esquerdo sobre o lector de pegadas dixitais"

#: pam/fingerprint-strings.h:56
#, c-format
msgid "Place your left thumb on %s"
msgstr "Dispoña o dedo polgar esquerdo en %s"

#: pam/fingerprint-strings.h:57
msgid "Swipe your left thumb across the fingerprint reader"
msgstr "Pase o seu dedo gordo esquerdo sobre o lector de pegadas dixitais"

#: pam/fingerprint-strings.h:58
#, c-format
msgid "Swipe your left thumb across %s"
msgstr "Pase o seu dedo gordo esquerdo sobre %s"

#: pam/fingerprint-strings.h:60
msgid "Place your left index finger on the fingerprint reader"
msgstr "Poña o seu dedo índice esquerdo sobre o lector de pegadas dixitais"

#: pam/fingerprint-strings.h:61
#, c-format
msgid "Place your left index finger on %s"
msgstr "Dispoña o seu dedo índice esquerdo en %s"

#: pam/fingerprint-strings.h:62
msgid "Swipe your left index finger across the fingerprint reader"
msgstr "Pase o seu dedo índice esquerdo sobre o lector de pegadas dixitais"

#: pam/fingerprint-strings.h:63
#, c-format
msgid "Swipe your left index finger across %s"
msgstr "Pase o seu dedo índice esquerdo sobre %s"

#: pam/fingerprint-strings.h:65
msgid "Place your left middle finger on the fingerprint reader"
msgstr "Poña o seu dedo corazón esquerdo sobre o lector de pegadas dixitais"

#: pam/fingerprint-strings.h:66
#, c-format
msgid "Place your left middle finger on %s"
msgstr "Dispoña o seu dedo mediano esquerdo en %s"

#: pam/fingerprint-strings.h:67
msgid "Swipe your left middle finger across the fingerprint reader"
msgstr "Pase o seu dedo corazón esquerdo sobre o lector de pegadas dixitais"

#: pam/fingerprint-strings.h:68
#, c-format
msgid "Swipe your left middle finger across %s"
msgstr "Pase o seu dedo corazón esquerdo sobre %s"

#: pam/fingerprint-strings.h:70
msgid "Place your left ring finger on the fingerprint reader"
msgstr "Poña o seu dedo anular dereito no lector de pegadas dixitais"

#: pam/fingerprint-strings.h:71
#, c-format
msgid "Place your left ring finger on %s"
msgstr "Dispoña o seu dedo anular esquerdo en %s"

#: pam/fingerprint-strings.h:72
msgid "Swipe your left ring finger across the fingerprint reader"
msgstr "Pase o seu dedo anular esquerdo polo lector de pegadas dixitais"

#: pam/fingerprint-strings.h:73
#, c-format
msgid "Swipe your left ring finger across %s"
msgstr "Poña o seu dedo anular esquerdo por %s"

#: pam/fingerprint-strings.h:75
msgid "Place your left little finger on the fingerprint reader"
msgstr "Poña o seu dedo anular esquerdo no lector de pegadas dixitais"

#: pam/fingerprint-strings.h:76
#, c-format
msgid "Place your left little finger on %s"
msgstr "Dispoña o seu dedo maimiño esquerdo en %s"

#: pam/fingerprint-strings.h:77
msgid "Swipe your left little finger across the fingerprint reader"
msgstr "Pase o seu dedo maimiño esquerdo polo lector de pegadas dixitais"

#: pam/fingerprint-strings.h:78
#, c-format
msgid "Swipe your left little finger across %s"
msgstr "Pase o seu dedo maimiño esquerdo por %s"

#: pam/fingerprint-strings.h:80
msgid "Place your right thumb on the fingerprint reader"
msgstr "Poña o seu dedo gordo dereito no lector de pegadas dixitais"

#: pam/fingerprint-strings.h:81
#, c-format
msgid "Place your right thumb on %s"
msgstr "Dispoña o seu dedo polgar dereito en %s"

#: pam/fingerprint-strings.h:82
msgid "Swipe your right thumb across the fingerprint reader"
msgstr "Pase o seu dedo gordo dereito polo lector de pegadas dixitais"

#: pam/fingerprint-strings.h:83
#, c-format
msgid "Swipe your right thumb across %s"
msgstr "Pase o seu dedo gordo dereito sobre %s"

#: pam/fingerprint-strings.h:85
msgid "Place your right index finger on the fingerprint reader"
msgstr "Poña o seu dedo índice dereito no lector de pegadas dixitais"

#: pam/fingerprint-strings.h:86
#, c-format
msgid "Place your right index finger on %s"
msgstr "Dispoña o seu dedo índice dereito en %s"

#: pam/fingerprint-strings.h:87
msgid "Swipe your right index finger across the fingerprint reader"
msgstr "Pase o seu dedo índice dereito sobre o lector de pegadas dixitais"

#: pam/fingerprint-strings.h:88
#, c-format
msgid "Swipe your right index finger across %s"
msgstr "Pase o seu dedo índice dereito sobre %s"

#: pam/fingerprint-strings.h:90
msgid "Place your right middle finger on the fingerprint reader"
msgstr "Poña o seu dedo corazón dereito no lector de pegadas dixitais"

#: pam/fingerprint-strings.h:91
#, c-format
msgid "Place your right middle finger on %s"
msgstr "Dispoña o seu dedo mediano dereito en %s"

#: pam/fingerprint-strings.h:92
msgid "Swipe your right middle finger across the fingerprint reader"
msgstr "Pase o seu dedo corazón dereito polo lector de pegadas dixitais"

#: pam/fingerprint-strings.h:93
#, c-format
msgid "Swipe your right middle finger across %s"
msgstr "Pase o seu dedo corazón dereito por %s"

#: pam/fingerprint-strings.h:95
msgid "Place your right ring finger on the fingerprint reader"
msgstr "Poña o seu dedo anular dereito no lector de pegadas dixitais"

#: pam/fingerprint-strings.h:96
#, c-format
msgid "Place your right ring finger on %s"
msgstr "Dispoña o seu dedo anular dereito en %s"

#: pam/fingerprint-strings.h:97
msgid "Swipe your right ring finger across the fingerprint reader"
msgstr "Pase o seu dedo anular dereito polo lector de pegadas dixitais"

#: pam/fingerprint-strings.h:98
#, c-format
msgid "Swipe your right ring finger across %s"
msgstr "Pase o seu dedo anular dereito por %s"

#: pam/fingerprint-strings.h:100
msgid "Place your right little finger on the fingerprint reader"
msgstr "Poña o seu dedo maimiño dereito sobre o lector de pegadas dixitais"

#: pam/fingerprint-strings.h:101
#, c-format
msgid "Place your right little finger on %s"
msgstr "Dispoña o seu dedo maimiño dereito en %s"

#: pam/fingerprint-strings.h:102
msgid "Swipe your right little finger across the fingerprint reader"
msgstr "Pase o seu dedo maimiño dereito sobre o lector de pegadas dixitais"

#: pam/fingerprint-strings.h:103
#, c-format
msgid "Swipe your right little finger across %s"
msgstr "Pase o seu dedo maimiño dereito sobre %s"

#: pam/fingerprint-strings.h:171 pam/fingerprint-strings.h:199
msgid "Place your finger on the reader again"
msgstr "Dispoña de novo o seu dedo no lector"

#: pam/fingerprint-strings.h:173 pam/fingerprint-strings.h:201
msgid "Swipe your finger again"
msgstr "Deslice de novo o seu dedo"

#: pam/fingerprint-strings.h:176 pam/fingerprint-strings.h:204
msgid "Swipe was too short, try again"
msgstr "A pasada foi moi curta, ténteo de novo"

#: pam/fingerprint-strings.h:178 pam/fingerprint-strings.h:206
msgid "Your finger was not centered, try swiping your finger again"
msgstr "O seu dedo non estaba centrado, tente a deslizalo de novo"

#: pam/fingerprint-strings.h:180 pam/fingerprint-strings.h:208
msgid "Remove your finger, and try swiping your finger again"
msgstr "Retire o seu dedo e tente deslizalo de novo"

#: pam/pam_fprintd.c:537
msgid "Verification timed out"
msgstr ""

#: pam/pam_fprintd.c:560
#, fuzzy
msgid "Failed to match fingerprint"
msgstr "Verificar a pegada dixital"

#: pam/pam_fprintd.c:576
msgid "An unknown error occurred"
msgstr ""
